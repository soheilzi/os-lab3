#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

void prog(){
    
    int i = 0;
    int num = 1000000;
    long long int result;
    long int op1, op2;
    int j = 0;

    op1 = 12345;
    op2 = 678910;
    while (1)
    {    
        for (i = 0; i < num; i++)
        {    
            for ( j = 0; j < num; j++)
            {
                result = i * j * op1 * op2;
            }
        }
        result += 1;    
    }
}

void prog1(){
    volatile unsigned long long i;
    for (i = 0; i < 1000000000ULL; ++i);
}


int main(int argc, char *argv[]){
    uint pid;

    pid = fork();

    if (pid == 0){
        prog();
    }else{
        pid = fork();
        if (pid == 0){
            prog1();
        }else{
            prog1();
            pid = fork();
            if (pid == 0){
                prog();
            }else{
                prog();
            }
        }
    }
    exit();
}
