#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}
/// lab 3 system calls
int sys_changeTheQueu(void)
{
  int pid;
  int priority;
  struct proc *p;
  if(argint(0, &pid) < 0 || argint(1, &priority) < 0)
    return -1;
  struct proc *table = get_all_procs();
  // acquire(table);
  for(p = table; p < &table[NPROC]; p++){
   	if (p->pid == pid){
      if(p->priority == RR)
        p->RRnum_is_set = 0;    
      p->priority = priority;
     }
	}
  // release(table);
  return 0;
}

int sys_initializeTicket(void)
{
  int pid;
  int ticket;
  struct proc *p;
  if(argint(0, &pid) < 0 || argint(1, &ticket) < 0)
    return -1;
  struct proc *table = get_all_procs();
  // acquire(table);
  for(p = table; p < &table[NPROC]; p++){
   	if (p->pid == pid)
      p->ticket = ticket;
	}
  // release(table);
  return 0;
}

int sys_BJFProcPars(void)
{ 
  int pid;
  int exec_ratio;
  int arrival_ratio;
  int priority_ratio;
  struct proc *p;
  if(argint(0, &pid) < 0 || argint(1, &priority_ratio) < 0 || argint(2, &arrival_ratio) < 0 || argint(3, &exec_ratio) < 0)
    return -1;
  struct proc *table = get_all_procs();
  // acquire(table);
  for(p = table; p < &table[NPROC]; p++){
   	if (p->pid == pid){
      p->prioity_ratio = priority_ratio;
      p->arrivalTime_raio = arrival_ratio;
      p->executedCycle_ratio = exec_ratio;
    }
	}
  return 0;
  // release(table);
}

int sys_BJFSysPars(void)
{
  int exec_ratio;
  int arrival_ratio;
  int priority_ratio;
  struct proc *p;
  if(argint(0, &priority_ratio) < 0 || argint(1, &arrival_ratio) < 0 || argint(2, &exec_ratio) < 0)
    return -1;
  struct proc *table = get_all_procs();
  // acquire(table);
  //set sysParam
  set_default_BJF_param(priority_ratio , arrival_ratio , exec_ratio);
  //
  for(p = table; p < &table[NPROC]; p++){
    p->prioity_ratio = priority_ratio;
    p->arrivalTime_raio = arrival_ratio;
    p->executedCycle_ratio = exec_ratio;
  // release(table);
  }
  return 0;
}

double calc_rank(struct proc* p){
  return ((double)p->prioity_ratio / p->ticket) + (p->arrivalTime * p->arrivalTime_raio) + (p->executedCycle * p->executedCycle_ratio);
}

int sys_printInfo(void)
{
  struct proc *p;
  struct proc *table = get_all_procs();
  char* state = 0;
  // acquire(table);
  cprintf("Name   Pid   State   Ticket  Priority   Ex-Cycles   Rank    PriorityR   ArrivalR    ExecutedR    Age\n");
  cprintf("........................................................................................................................\n");
  for(p = table; p < &table[NPROC]; p++){
    if(p->state != UNUSED){
      if(p->state == EMBRYO)
          state = "EMBRYO";
      else if(p->state == SLEEPING)
          state = "SLEEPING";
      else if(p->state == RUNNABLE)
          state = "RUNNABLE";
      else if(p->state == RUNNING)
          state = "RUNNING";
      else if(p->state == ZOMBIE)
          state = "ZOMBIE";

      cprintf("%s\t%d\t%s\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n",
        p->name, p->pid, state, p->ticket, p->priority, (int)p->executedCycle, (int)calc_rank(p), p->prioity_ratio, p->arrivalTime_raio, p->executedCycle_ratio, p->age);
    
    }
  }
  return 0;
}