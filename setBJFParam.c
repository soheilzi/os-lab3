#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

int main(int argc, char *argv[]){
    if(argc != 5)
        printf(0 , "Not Enough Arguments!\n");
    int pid = atoi(argv[1]);
    int ratio1 = atoi(argv[2]);
    int ratio2 = atoi(argv[3]);
    int ratio3 = atoi(argv[4]);
	BJFProcPars(pid , ratio1 , ratio2 , ratio3);
    exit();
}
