#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

int main(int argc, char *argv[]){
    if(argc != 3)
        printf(0 , "Not Enough Arguments!\n");
    int pid = atoi(argv[1]);
    int ticket = atoi(argv[2]);
	initializeTicket(pid , ticket);
    exit();
}
